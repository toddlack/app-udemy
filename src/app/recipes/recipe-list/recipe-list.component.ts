import { Component, OnInit } from '@angular/core';
import {Recipe} from "../recipe.model";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes : Recipe[] = [
    new Recipe('A Test Recipe','this is a test','http://clv.h-cdn.co/assets/16/15/980x490/landscape-1460386745-ground-beef-recipes.jpg'),
    new Recipe('Good Recipe','Second','http://clv.h-cdn.co/assets/16/15/980x490/landscape-1460386745-ground-beef-recipes.jpg'),
    new Recipe('Great Recipe','this is a test','http://clv.h-cdn.co/assets/16/15/980x490/landscape-1460386745-ground-beef-recipes.jpg')
    ];

  constructor() { }

  ngOnInit() {
  }

}
